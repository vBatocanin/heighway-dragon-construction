# The Heighway Dragon #

This repo tackles the problem of constructing the [Heighway dragon](https://en.wikipedia.org/wiki/Dragon_curve) fractal utilizing only clockwise and counterclockwise folds.
The desired result can be seen [here](https://en.wikipedia.org/wiki/Dragon_curve#/media/File:Dragon_Curve_adding_corners_trails_rectangular_numbered_R.gif).

This animation was done in the [GCLC](http://poincare.matf.bg.ac.rs/~janicic/gclc/) programming language.
